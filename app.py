from flask import Flask
from apscheduler.schedulers.background import BackgroundScheduler
import logging
from os import getenv
import pyodbc

app = Flask(__name__)
app.debug = True

INTERVAL_IN_SEC = 10

server = getenv("DB_SERVER")
user = getenv("DB_USERNAME")
password = getenv("DB_PASSWORD")
dbname = getenv("DB_NAME")

log = logging.getLogger('apscheduler.executors.default')
log.setLevel(logging.INFO)
fmt = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
h = logging.StreamHandler()
h.setFormatter(fmt)
log.addHandler(h)

def try_to_connect():
    try:
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=%s;DATABASE=%s;UID=%s;PWD=%s' % (server,dbname,user,password))
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM %s.INFORMATION_SCHEMA.TABLES;' % dbname)
        row = cursor.fetchone()
        while row:
            log.info(row)
            row = cursor.fetchone()
        conn.close()
    except Exception as e:
        raise e
    

@app.route("/")
def main():
    return "Hello World!"
 
if __name__ == "__main__":
    scheduler = BackgroundScheduler()
    scheduler.add_job(try_to_connect, 'interval', seconds=INTERVAL_IN_SEC)
    scheduler.start()
    app.run(threaded=True)

